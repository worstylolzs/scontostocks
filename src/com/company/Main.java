package com.company;

public class Main {

    public static void main(String[] args) {
        String stocksFromIm="1,4,3;" +// Můj urpavený formát informací, který bych chtěl aby chodil z můstku a napsal bych ještě program, který by ty informace takto upravil.
                "3,4,7;" +// První číslo:Sklad, Druhé číslo: počet, třetí číslo: týden jíž se započtenou rezervou. Tyto údaje jsou na všechny sklady daného produktu.
                "2,3,5;" +//Představuji si, že by mi na jeden produkt přišel dlouhý, který by měl záznamy rozdělené třeba tečkou"." a ty bych si pak upravil do této podoby.
                "1,1,2;" +//
                "2,4,1;" +
                "1,4,1;" +
                "3,2,4;" +
                "2,4,3;";
        String stocksInPole[][]=new String[3][10];// vytvoření vícerozměrné tabulky, aktuálně je zde zadáno na pevno velikost, jinak by se hodnota prvního pole brala-
        for (int q=0;q<3;q++){//dle maximální hotnoty id skladu co mi příjde, opět bych si nechal vyhledat v záznamu co bym i přišel z můstku, nejvyšší hodnotu ID skladu, co příjdou.
            for (int w=0;w<10;w++){//druhá hodnota, je nejvyšší týden dodání co příjde, opět bych vyfiltroval stejně jako první hodnotu
                stocksInPole[q][w]="0";
//                System.out.printf("%s",stocksInPole[q][w]);
            }
//            System.out.println(" ");
        }
        String stockInPoleEdited[]=stocksFromIm.split(";");//tento for mi zapíše hodnoty do tabulky, se kterou násldně pracuji dle podmínek.
        for (String s:stockInPoleEdited){
            String pole[]=s.split(",");
            int poleOne=Integer.parseInt(pole[0])-1;
            int poleTwo=Integer.parseInt(pole[2])-1;
            stocksInPole[poleOne][poleTwo]=pole[1];
            }
        System.out.println("Po editaci zadání skladů do polí:");// zobrazení, jak je tabulka naplněná po zadání údajů. Pokud je údaj prázdný, tak má nulu.
        for (int a=0;a<3;a++){
            for (int b=0;b<10;b++){
                System.out.print(stocksInPole[a][b]);
            }
            System.out.println("");
            }
        int count=18;// počet kusů, které si člověk zadal na FE
        int counterToEnd=count;// counterToEnd Má stejnou hodnotu jako count. Při nalezení počku zboží na sklade, se tento počet z tohoto intu odečte, to poté způsobí break a ukončí hledání.
        int betweenStock=2;// převoz mezi sklady
        int betweenStockTimer=0;// pridávač týdnů, pokud nastane situace, že mám 14 převoz mezi sklady, chci dva kusy a jeden dorazi 1 týden a druhý 2 týden, tak nám to připočte dopravu prvního kusu k druhému.
        int weekFutureNeeder=count;//tento int se používá na pokud první týden nesplní počet kusů, tak zbyvající počet kusů, co potřebuji další týdny se zapíše sem. Následně nám to každý týden odečítá, dokud nebude mít 0.
        String inStockAfterFutureCalculate[]=new String[3];//pole, kde zapisuji výsledky dostupností aktuálního skladu, mělo by to mít dleku pole počtu skladů.
        int whatWeekBetweenTimerEnds=0;
        for (int d=0;d<10;d++){//for na týdny.
            int thisWeekCounter=0;// počítá, kolik kusů máme na skladech v určitém týdnu, pozdějí slouží k breaku cyklu.
            for (int dd=0;dd<3;dd++){// for na týdny, co kontroluje, jednotlivé sklady v každém týdnu
                int countInsidePole=Integer.parseInt(stocksInPole[dd][d]);// hodnota v poli.
                thisWeekCounter+=countInsidePole;
                int changer=0;
                if(weekFutureNeeder!=0){
                changer=countInsidePole/weekFutureNeeder;}// zde jsem mohl dát podmínku nebo boolean, chci vědět zdali changer bude changer>=1, pokud ano tak je na tomto skladě dotačující zboží.
                counterToEnd-=countInsidePole;//tento int mi hlídá, jestli jsme našli už všechny kusy co potřebujeme. Pokud ano, tak poté zastaví cyklus přes break. Zastavuje ho až po zkontrolování celého týdne.
                if((changer>=1)) {
                    if(d==0) {// tato podmínka je na situaci, kdy je první týden zboží skladem.
                        inStockAfterFutureCalculate[dd] = String.valueOf(d);
                    }else {
                    inStockAfterFutureCalculate[dd] = String.valueOf(d+1);//pokud se zboží skladem najde jiný týden než první, tak zde pošle číslo aktuálního týdne. +1 jelikož beru čísla z for, který začíná nulou.
                    }
                }else {
//                   if(d==0) {inStockAfterFutureCalculate[dd]=String.valueOf((betweenStock+d+1));
//                   }else {inStockAfterFutureCalculate[dd]=String.valueOf((betweenStock+d));
//                       }
                    inStockAfterFutureCalculate[dd]=String.valueOf((betweenStock+d+1));//jednička se opět přidavá kuli For
                   }
//                betweenStockTimer--;
//                if(betweenStockTimer<0){
//                    betweenStockTimer=0;
//                }
            }
            betweenStockTimer-=1;// tyto funkce slouží k produktům, které se posílají z minulého týdne, ale nedorazí, kuli tomu že přesun je 2 týdny a ne 1. týdny se dávají do rezervy, aby se mohli připočíst.
            if(betweenStockTimer<0){
                betweenStockTimer=0;
            }
            weekFutureNeeder-=thisWeekCounter;// z toho co jsme potřebovali na týden, odečteme co jsme dostali.
            if(counterToEnd<=0){// pokud máme vše, tak ukončíme proces hledání
                break;
            }else if(counterToEnd>0&&thisWeekCounter>0){//pokud jsme něco tento týden nalezli, ale nesplnilo to počet co potřebujeme do konce, tak zde vytvoříme rezervu týdnů co musíme připočíst.
                betweenStockTimer=betweenStock;
                whatWeekBetweenTimerEnds=betweenStockTimer+d+1;//zde si určíme, v jakém týdnu má tento počet skončit, jelikož nám to pak ovlivní, zdali použít tyto týdny nebo ne.
            }

        }
        System.out.println("Dostupnost Skladů na "+count+" počtu zboží");//klasický výpis počtu kusů.
        for(String boo:inStockAfterFutureCalculate){
//            try{
            int test=Integer.parseInt(boo);
            if(test>0&&whatWeekBetweenTimerEnds>test){// pokud bychom měli hodnotu týdnů kdy zboží dorazí menší, než kdy má timer týdnů skončit, tak nám týdny připočte.
                System.out.println("K dispozici za "+(test+betweenStockTimer)+" týdnů.");
            }else if(test==0) {
                System.out.println("Skladem");
            }else {
                System.out.println("K dispozici za "+(test)+" týdnů.");
            }
//            }} catch (NumberFormatException e) {
//                System.out.println(boo);
            }
        }

        }


//        for (int a=0;a<3;a++){
//
//        }





